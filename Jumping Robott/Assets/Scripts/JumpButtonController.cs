﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpButtonController : MonoBehaviour
{
    public Material ObjMat;
    float timeCooldown;
    public Animator animcia;

    private void Start() 
    {
        // Na starcie wczytuję cooldown o wartości 4 sek (tyle trwa animacja) i ustawia kolor na zielony. 
        timeCooldown = (4.0f);
        ObjMat.color = Color.green;
    }
    public void OnMouseDown()
    { 
        // Po wciśnięciu myszki ma ustawić bool na true ( w unity ustawiłem aby animacja odpalała się poprzez bool'a o nazwie "anim"), następnie zmienia przycisk na kolor czerwony.
        animcia.SetBool("anim", true);
        ObjMat.color = Color.red;
    }
    public void Update()
    {
        // Jeżeli bool jest na true.
        if (animcia.GetBool("anim") == true)
        {
            // Odlicza cooldown, jeżeli spadnie poniżej 0 to...
            timeCooldown -= Time.deltaTime;
            if (timeCooldown < 0)
            {
                // Ustawia bool'a spowrotem na false, dzięki czemu animacja wraca do Idle i ponownie wczytuje cooldown.
                animcia.SetBool("anim", false);
                timeCooldown = (4.0f);
            }
        }
    }
    public void FixedUpdate()
    {
        // Po powyższym Update, sprawdza czy Bool jest ustawiony na false.
        if (animcia.GetBool("anim") == false)
        {
            // I zmienia kolor na zielony dając informację, że można ponownie wywołać skok.
            ObjMat.color = Color.green;
        }
    }
}