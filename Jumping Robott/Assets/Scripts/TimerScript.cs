﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    public Text timer;
    // Przy wciśnięciu przycisku.
    public void Button_OnClickk()
    {
        // Jeżeli timer jest włączony.
        if (timer.enabled == true)
        {
            // to ma go wyłączyć.
            timer.enabled = false;
        }
        else
        {
            // Jeżeli nie to ma go włączyć.
            timer.enabled = true;
        }
	}
}
