﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomSounds : MonoBehaviour
{
    // Wczytanie tekstu /ARCamera/Canvas/ButtonsSoundTimer.
    public Text czas;
    private SkinnedMeshRenderer robot;
    int mnumberclip;
    float timeCooldown;
    SkinnedMeshRenderer m_Renderer;
    // Publiczny array do którego wrzucone są 3 dźwięki.
    public AudioClip[] list;
    private AudioSource source;
    void Start()
    {
        // Wczytuję komponent AudioSource oraz SkinnedMeshRenderer z objektu Robot2.
        source = GetComponent<AudioSource>();
        m_Renderer = GetComponent<SkinnedMeshRenderer>();
        // Losowanie liczby do odtworzenia jednego z 3 dźwięków.
        mnumberclip = Random.Range(0, 3);
        // Losowanie czasu dla przedziału 5-10 sek.
        timeCooldown = Random.Range(5, 11);
        // Konwersja wybranego czasu do Stringa aby wyświetlić go w tekście.Zaokrągla do 2 miejsc po przecinku "#.00".
        czas.text = timeCooldown.ToString("#.00");
    }
    void Update()
    {
        // Jeżeli robot pojawi się w zasięgu widzenia kamery.
        if (m_Renderer.isVisible)
        {
            // Czeka aż minie czas wcześniej zdefiniowany.
            timeCooldown -= Time.deltaTime;
            if (timeCooldown < 0)
            {
                // Odtwarza raz dźwięk wybrany wcześniej.
                source.PlayOneShot(list[mnumberclip]);
                // I od nowa wybiera nowy clip oraz nowy cooldown.
                mnumberclip = Random.Range(0, 3);
                timeCooldown = Random.Range(5, 11);
            }
        }
        // Ponownie konwertuje cooldown do tekstu. Zaokrągla do 2 miejsc po przecinku "#.00".
        czas.text = timeCooldown.ToString("#.00");
    }
}

