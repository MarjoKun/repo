﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingText : MonoBehaviour {
    public Camera cam;

	void Update ()
    {
        // Określenie róznicy między pozycją kamery a tekstu.
        Vector3 v = cam.transform.position - transform.position;
        // Nie do końca rozumiem, ale chyba ma domyślnie x oraz z ustawić na 0.
        v.x = v.z = 0.0f;
        // O ile dobrze rozumiem ustawia pozycję tekstu oraz ustawia domyślnie tekst na 180 bo byłby odwrócony.
        transform.LookAt(cam.transform.position - v);
        transform.Rotate(0, 180, 0);
    }
}
